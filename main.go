package main

import (
	"fmt"
	"strings"
	"time"

	appenginepb "cloud.google.com/go/appengine/apiv1/appenginepb"
	"github.com/goccy/go-yaml"
	"google.golang.org/protobuf/types/known/durationpb"
)

// AppYaml represents the structure of an app.yaml file.
type AppYaml struct {
	Runtime            string              `yaml:"runtime"`
	Service            string              `yaml:"service"` //TODO: use this field
	Env                string              `yaml:"env"`
	BuildEnvVariables  map[string]string   `yaml:"build_env_variables"`
	DefaultExpiration  string              `yaml:"default_expiration"`
	Entrypoint         string              `yaml:"entrypoint"`
	EnvVariables       map[string]string   `yaml:"env_variables"`
	ErrorHandlers      []*ErrorHandler     `yaml:"error_handlers"`
	Handlers           []*Handler          `yaml:"handlers"`
	InboundServices    []string            `yaml:"inbound_services"`
	InstanceClass      string              `yaml:"instance_class"`
	ServiceAccount     string              `yaml:"service_account"`
	VpcAccessConnector *VpcAccessConnector `yaml:"vpc_access_connector"`
	AutomaticScaling   *AutomaticScaling   `yaml:"automatic_scaling"`
	ManualScaling      *ManualScaling      `yaml:"manual_scaling"`
	BasicScaling       *BasicScaling       `yaml:"basic_scaling"`
}

// VpcAccessConnector struct
type VpcAccessConnector struct {
	Name          string `yaml:"name"`
	EgressSetting string `yaml:"egress_setting"`
}

// AutomaticScaling defines automatic scaling settings.
type AutomaticScaling struct {
	StandardSchedulerSettings *StandardSchedulerSettings `yaml:"standard_scheduler_settings"`
	MaxIdleInstances          int32                      `yaml:"max_idle_instances"`
	MinIdleInstances          int32                      `yaml:"min_idle_instances"`
	MaxConcurrentRequests     int32                      `yaml:"max_concurrent_requests"`
	MinPendingLatency         string                     `yaml:"min_pending_latency"`
	MaxPendingLatency         string                     `yaml:"max_pending_latency"`
}

// StandardSchedulerSettings defines settings specific to the standard scheduler.
type StandardSchedulerSettings struct {
	MaxInstances                int32   `yaml:"max_instances"`
	MinInstances                int32   `yaml:"min_instances"`
	TargetCPUUtilization        float64 `yaml:"target_cpu_utilization"`
	TargetThroughputUtilization float64 `yaml:"target_throughput_utilization"`
}

// BasicScaling defines settings for basic scaling.
type BasicScaling struct {
	IdleTimeout  string `yaml:"idle_timeout"` // e.g., "10m"
	MaxInstances int32  `yaml:"max_instances"`
}

// ManualScaling defines manual scaling settings.
type ManualScaling struct {
	Instances int32 `yaml:"instances"`
}

// Handler defines a URL handler.
type Handler struct {
	AuthFailAction           string            `yaml:"auth_fail_action"`
	URL                      string            `yaml:"url"`
	StaticFiles              string            `yaml:"static_files"`
	Upload                   string            `yaml:"upload"`
	Script                   string            `yaml:"script"`
	Login                    string            `yaml:"login"`
	Secure                   string            `yaml:"secure"`
	StaticDir                string            `yaml:"static_dir"`
	MimeType                 string            `yaml:"mime_type"`
	Expiration               string            `yaml:"expiration"`
	HttpHeaders              map[string]string `yaml:"http_headers"`
	RedirectHTTPResponseCode int               `yaml:"redirect_http_response_code"`
}

// ErrorHandler defines an error handler.
type ErrorHandler struct {
	ErrorCode  string `yaml:"error_code"`
	StaticFile string `yaml:"file"`
}

// ParseAppYaml parses an app.yaml file from a byte slice.
func ParseAppYaml(data []byte) (*AppYaml, error) {
	appYaml := &AppYaml{}
	err := yaml.Unmarshal(data, appYaml)
	if err != nil {
		return nil, fmt.Errorf("error parsing app.yaml: %v", err)
	}
	return appYaml, nil
}

// durationFromYamlString converts a YAML string like "30ms" to time.Duration
func durationFromYamlString(yamlStr string) *durationpb.Duration {
	if yamlStr == "" || yamlStr == "automatic" {
		return nil // Return nil for empty or "automatic" values
	}
	duration, err := time.ParseDuration(yamlStr)
	if err != nil {
		fmt.Printf("Warning: Invalid duration string: %s\n", yamlStr)
		return nil
	}
	return durationpb.New(duration)
}

// MapToVersion maps parsed app.yaml data to an appenginepb.Version.
func MapToVersion(appYaml *AppYaml) *appenginepb.Version {
	version := &appenginepb.Version{
		Runtime:       appYaml.Runtime,
		Env:           appYaml.Env,
		InstanceClass: appYaml.InstanceClass,
		EnvVariables:  appYaml.EnvVariables,
	}

	// Build Environment Variables
	version.BuildEnvVariables = appYaml.BuildEnvVariables

	// Default Expiration
	version.DefaultExpiration = durationFromYamlString(appYaml.DefaultExpiration)

	// Entrypoint
	if appYaml.Entrypoint != "" {
		version.Entrypoint = &appenginepb.Entrypoint{
			Command: &appenginepb.Entrypoint_Shell{
				Shell: appYaml.Entrypoint,
			},
		}
	}

	// Inbound Services
	version.InboundServices = make([]appenginepb.InboundServiceType, len(appYaml.InboundServices))
	for i, service := range appYaml.InboundServices {
		if service == "warmup" {
			version.InboundServices[i] = appenginepb.InboundServiceType_INBOUND_SERVICE_WARMUP
		} else {
			fmt.Printf("Warning: Invalid inbound service: %s\n", service)
		}
	}

	// Service Account
	version.ServiceAccount = appYaml.ServiceAccount

	// Vpc Access Connector
	if appYaml.VpcAccessConnector != nil {
		egressSetting, ok := appenginepb.VpcAccessConnector_EgressSetting_value[strings.ToUpper(appYaml.VpcAccessConnector.EgressSetting)]
		if !ok {
			fmt.Printf("Warning: Invalid egress setting: %s\n", appYaml.VpcAccessConnector.EgressSetting)
		} else {
			version.VpcAccessConnector = &appenginepb.VpcAccessConnector{
				Name:          appYaml.VpcAccessConnector.Name,
				EgressSetting: appenginepb.VpcAccessConnector_EgressSetting(egressSetting),
			}
		}
	}

	// Automatic Scaling
	if appYaml.AutomaticScaling != nil {
		automaticScaling := &appenginepb.AutomaticScaling{
			MaxIdleInstances:      appYaml.AutomaticScaling.MaxIdleInstances,
			MinIdleInstances:      appYaml.AutomaticScaling.MinIdleInstances,
			MaxConcurrentRequests: appYaml.AutomaticScaling.MaxConcurrentRequests,
			MinPendingLatency:     durationFromYamlString(appYaml.AutomaticScaling.MinPendingLatency),
			MaxPendingLatency:     durationFromYamlString(appYaml.AutomaticScaling.MaxPendingLatency),
		}

		// Standard Scheduler Settings
		if appYaml.AutomaticScaling.StandardSchedulerSettings != nil {
			automaticScaling.StandardSchedulerSettings = &appenginepb.StandardSchedulerSettings{
				MaxInstances:                appYaml.AutomaticScaling.StandardSchedulerSettings.MaxInstances,
				MinInstances:                appYaml.AutomaticScaling.StandardSchedulerSettings.MinInstances,
				TargetCpuUtilization:        appYaml.AutomaticScaling.StandardSchedulerSettings.TargetCPUUtilization,
				TargetThroughputUtilization: appYaml.AutomaticScaling.StandardSchedulerSettings.TargetThroughputUtilization,
			}
		}

		version.Scaling = &appenginepb.Version_AutomaticScaling{
			AutomaticScaling: automaticScaling,
		}
	}

	// Basic Scaling
	if appYaml.BasicScaling != nil {
		basicScaling := &appenginepb.BasicScaling{
			IdleTimeout:  durationFromYamlString(appYaml.BasicScaling.IdleTimeout),
			MaxInstances: appYaml.BasicScaling.MaxInstances,
		}
		version.Scaling = &appenginepb.Version_BasicScaling{
			BasicScaling: basicScaling,
		}
	}

	// Manual Scaling
	if appYaml.ManualScaling != nil {
		version.Scaling = &appenginepb.Version_ManualScaling{
			ManualScaling: &appenginepb.ManualScaling{
				Instances: appYaml.ManualScaling.Instances,
			},
		}
	}

	// Handlers
	version.Handlers = make([]*appenginepb.UrlMap, len(appYaml.Handlers))
	for i, handler := range appYaml.Handlers {
		urlMap := &appenginepb.UrlMap{
			UrlRegex: handler.URL,
		}

		// Determine the handler type and assign it correctly
		if handler.StaticFiles != "" || handler.StaticDir != "" {
			expiration := durationFromYamlString(handler.Expiration)
			urlMap.HandlerType = &appenginepb.UrlMap_StaticFiles{
				StaticFiles: &appenginepb.StaticFilesHandler{
					Expiration:  expiration,
					HttpHeaders: handler.HttpHeaders,
					// Important: No UploadPathRegex for StaticDir
					// The StaticDir path is set in the UrlRegex of the UrlMap
				},
			}
			if handler.StaticDir != "" {
				urlMap.HandlerType = &appenginepb.UrlMap_StaticFiles{
					StaticFiles: &appenginepb.StaticFilesHandler{
						Path:        handler.StaticDir,
						Expiration:  expiration,
						MimeType:    handler.MimeType,
						HttpHeaders: handler.HttpHeaders,
					},
				}
			} else {
				urlMap.HandlerType.(*appenginepb.UrlMap_StaticFiles).StaticFiles.UploadPathRegex = handler.Upload
			}
		} else if handler.Script == "auto" { // Check for "auto" only
			urlMap.HandlerType = &appenginepb.UrlMap_Script{
				Script: &appenginepb.ScriptHandler{
					ScriptPath: "auto", // Set ScriptPath to "auto"
				},
			}
		}
		// Security Level (if set)
		if handler.Secure != "" {
			securityLevel, ok := appenginepb.SecurityLevel_value[strings.ToUpper("secure_"+handler.Secure)]
			if !ok {
				fmt.Printf("Warning: Invalid security level: %s\n", handler.Secure)
			} else {
				urlMap.SecurityLevel = appenginepb.SecurityLevel(securityLevel)
			}
		}

		// Redirect HTTP Response Code (if set)
		if handler.RedirectHTTPResponseCode != 0 {
			urlMap.RedirectHttpResponseCode = appenginepb.UrlMap_RedirectHttpResponseCode(handler.RedirectHTTPResponseCode)
		}

		// Set AuthFailAction in the UrlMap
		if handler.AuthFailAction != "" {
			authFailAction, ok := appenginepb.AuthFailAction_value["AuthFailAction_AUTH_FAIL_ACTION_"+strings.ToUpper(handler.AuthFailAction)]
			if !ok {
				fmt.Printf("Warning: Invalid auth_fail_action: %s\n", handler.AuthFailAction)
			} else {
				urlMap.AuthFailAction = appenginepb.AuthFailAction(authFailAction)
			}
		}

		urlMap.Login = appenginepb.LoginRequirement(appenginepb.LoginRequirement_value["LoginRequirement_LOGIN_"+strings.ToUpper(handler.Login)])

		version.Handlers[i] = urlMap
	}

	// Error Handlers
	version.ErrorHandlers = make([]*appenginepb.ErrorHandler, len(appYaml.ErrorHandlers))
	for i, errHandler := range appYaml.ErrorHandlers {
		errorCode, ok := appenginepb.ErrorHandler_ErrorCode_value[strings.ToUpper("error_code_"+errHandler.ErrorCode)]
		if !ok {
			fmt.Printf("Warning: Invalid error code: %s\n", errHandler.ErrorCode)
			continue // Skip to the next error handler
		}
		version.ErrorHandlers[i] = &appenginepb.ErrorHandler{
			ErrorCode:  appenginepb.ErrorHandler_ErrorCode(errorCode), // Correct conversion
			StaticFile: errHandler.StaticFile,
		}
	}

	return version
}

func main() {
	// Example usage
	appYamlData := []byte(`
runtime: go122
env: flexible
instance_class: F1
handlers:
  - url: /.*
    script: _go_app
  - url: /static
    static_dir: static
`)
	appYaml, err := ParseAppYaml(appYamlData)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Parsed app.yaml:\n%+v\n", appYaml)
	version := MapToVersion(appYaml)
	fmt.Printf("Mapped Version:\n%+v\n", version)
}
