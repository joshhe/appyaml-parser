package main

import (
	"strings"
	"testing"
	"time"

	appenginepb "cloud.google.com/go/appengine/apiv1/appenginepb"
	"github.com/google/go-cmp/cmp"
	"google.golang.org/protobuf/testing/protocmp"
	"google.golang.org/protobuf/types/known/durationpb"
)

func TestParseAndMapToVersion(t *testing.T) {
	testCases := []struct {
		name     string
		yamlData string
		want     *appenginepb.Version
		wantErr  bool
		errorMsg string
	}{
		{
			name: "EnvField",
			yamlData: `
runtime: go122
env: standard
`,
			want: &appenginepb.Version{
				Runtime: "go122",
				Env:     "standard",
			},
		},
		{
			name: "Basic",
			yamlData: `
runtime: go122
env: flexible
instance_class: F1
handlers:
  - url: /.*
    script: auto
  - url: /static
    static_dir: static
    mime_type: text/html
    expiration: 24h
`,
			want: &appenginepb.Version{
				Runtime:       "go122",
				Env:           "flexible",
				InstanceClass: "F1",
				Handlers: []*appenginepb.UrlMap{
					{
						UrlRegex: "/.*",
						HandlerType: &appenginepb.UrlMap_Script{
							Script: &appenginepb.ScriptHandler{ScriptPath: "auto"},
						},
					},
					{
						UrlRegex: "/static",
						HandlerType: &appenginepb.UrlMap_StaticFiles{
							StaticFiles: &appenginepb.StaticFilesHandler{
								Expiration: durationpb.New(time.Hour * 24),
								Path:       "static",
								MimeType:   "text/html",
							},
						},
					},
				},
			},
		},
		{
			name: "AutomaticScaling",
			yamlData: `
automatic_scaling:
  standard_scheduler_settings:
    max_instances: 10
    min_instances: 2
    target_cpu_utilization: 0.8
    target_throughput_utilization: 0.5
  max_idle_instances: 5
  min_idle_instances: 1
  max_concurrent_requests: 20
  min_pending_latency: 50ms
  max_pending_latency: 100ms
`,
			want: &appenginepb.Version{
				Scaling: &appenginepb.Version_AutomaticScaling{
					AutomaticScaling: &appenginepb.AutomaticScaling{
						StandardSchedulerSettings: &appenginepb.StandardSchedulerSettings{
							MaxInstances:                10,
							MinInstances:                2,
							TargetCpuUtilization:        0.8,
							TargetThroughputUtilization: 0.5,
						},
						MaxIdleInstances:      5,
						MinIdleInstances:      1,
						MaxConcurrentRequests: 20,
						MinPendingLatency:     durationpb.New(time.Millisecond * 50),
						MaxPendingLatency:     durationpb.New(time.Millisecond * 100),
					},
				},
			},
		},
		{
			name: "BasicScaling",
			yamlData: `
basic_scaling:
  max_instances: 5
  idle_timeout: 10m
`,
			want: &appenginepb.Version{
				Scaling: &appenginepb.Version_BasicScaling{
					BasicScaling: &appenginepb.BasicScaling{
						MaxInstances: 5,
						IdleTimeout:  durationpb.New(time.Minute * 10),
					},
				},
			},
		},
		{
			name: "ManualScaling",
			yamlData: `
manual_scaling:
  instances: 3
`,
			want: &appenginepb.Version{
				Scaling: &appenginepb.Version_ManualScaling{
					ManualScaling: &appenginepb.ManualScaling{
						Instances: 3,
					},
				},
			},
		},
		{
			name: "EnvVariables",
			yamlData: `
env_variables:
  VAR1: value1
  VAR2: value2
`,
			want: &appenginepb.Version{
				EnvVariables: map[string]string{
					"VAR1": "value1",
					"VAR2": "value2",
				},
			},
		},
		{
			name: "ErrorHandlers",
			yamlData: `
error_handlers:
  - error_code: over_quota
    file: over_quota.html
  - error_code: timeout
    file: default.html
`,
			want: &appenginepb.Version{
				ErrorHandlers: []*appenginepb.ErrorHandler{
					{
						ErrorCode:  appenginepb.ErrorHandler_ERROR_CODE_OVER_QUOTA,
						StaticFile: "over_quota.html",
					},
					{
						ErrorCode:  appenginepb.ErrorHandler_ERROR_CODE_TIMEOUT,
						StaticFile: "default.html",
					},
				},
			},
		},
		{
			name: "HandlersComplete",
			yamlData: `
handlers:
  - url: /images
    static_dir: static/images
    expiration: 48h
    http_headers:
      Cache-Control: public, max-age=86400
  - url: /static
    static_files: static
    upload: static/.+\.jpg
    secure: always
    redirect_http_response_code: 301
`,
			want: &appenginepb.Version{
				Handlers: []*appenginepb.UrlMap{
					{
						UrlRegex: "/images",
						HandlerType: &appenginepb.UrlMap_StaticFiles{
							StaticFiles: &appenginepb.StaticFilesHandler{
								Expiration:  durationpb.New(time.Hour * 48),
								HttpHeaders: map[string]string{"Cache-Control": "public, max-age=86400"},
								Path:        "static/images",
							},
						},
					},
					{
						UrlRegex: "/static",
						HandlerType: &appenginepb.UrlMap_StaticFiles{
							StaticFiles: &appenginepb.StaticFilesHandler{
								UploadPathRegex: `static/.+\.jpg`,
							},
						},
						SecurityLevel:            appenginepb.SecurityLevel_SECURE_ALWAYS,
						RedirectHttpResponseCode: appenginepb.UrlMap_RedirectHttpResponseCode(301),
					},
				},
			},
		},
		{
			name: "AdditionalFields",
			yamlData: `
runtime: go122
build_env_variables:
  VAR1: build_value1
default_expiration: 1h
entrypoint: go run main.go
inbound_services:
  - warmup
service_account: my-service-account@project.iam.gserviceaccount.com
vpc_access_connector:
  name: connector-name
  egress_setting: ALL_TRAFFIC
`,
			want: &appenginepb.Version{
				Runtime: "go122",
				BuildEnvVariables: map[string]string{
					"VAR1": "build_value1",
				},
				DefaultExpiration: durationpb.New(time.Hour),
				Entrypoint: &appenginepb.Entrypoint{
					Command: &appenginepb.Entrypoint_Shell{
						Shell: "go run main.go",
					},
				},
				InboundServices: []appenginepb.InboundServiceType{appenginepb.InboundServiceType_INBOUND_SERVICE_WARMUP},
				ServiceAccount:  "my-service-account@project.iam.gserviceaccount.com",
				VpcAccessConnector: &appenginepb.VpcAccessConnector{
					Name:          "connector-name",
					EgressSetting: appenginepb.VpcAccessConnector_ALL_TRAFFIC,
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			appYaml, err := ParseAppYaml([]byte(tc.yamlData))
			if err != nil {
				if !tc.wantErr {
					t.Errorf("ParseAppYaml() unexpected error: %v", err)
				} else if !strings.Contains(err.Error(), tc.errorMsg) {
					t.Errorf("ParseAppYaml() error message mismatch:\nGot:  %v\nWant: %v", err.Error(), tc.errorMsg)
				}
				return // Stop test if parsing error was expected
			}

			got := MapToVersion(appYaml)
			if diff := cmp.Diff(tc.want, got, protocmp.Transform()); diff != "" {
				t.Errorf("MapToVersion() mismatch (-want +got):\n%s", diff)
			}
		})
	}
}
