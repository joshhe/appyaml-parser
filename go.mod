module gitlab.com/joshhe/appyaml-parser

go 1.21

require (
	cloud.google.com/go/appengine v1.8.7
	github.com/goccy/go-yaml v1.11.3
	github.com/google/go-cmp v0.6.0
	google.golang.org/protobuf v1.34.0
)

require (
	cloud.google.com/go/longrunning v0.5.6 // indirect
	github.com/fatih/color v1.10.0 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	golang.org/x/net v0.24.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/genproto/googleapis/api v0.0.0-20240429193739-8cf5692501f6 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240429193739-8cf5692501f6 // indirect
	google.golang.org/grpc v1.63.2 // indirect
)
